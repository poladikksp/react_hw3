import { ItemList } from "../components/ItemsList/ItemsList";

export function Home({ items, favItems, addToFav, removeFromFav, openModal }) {
  return (
    <>
      <ItemList
        openModal={openModal}
        addToFav={addToFav}
        removeFromFav={removeFromFav}
        items={items}
        favItems={favItems}
      />
    </>
  );
}
