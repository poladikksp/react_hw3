import { ItemList } from "../components/ItemsList/ItemsList";

export function Cart(props) {
  return (
    <>
      <h3 className="text-center pt-3 pb-2">Your cart:</h3>
      {props.cartItems.length === 0 ? <p className="text-center fs-4">Empty...</p> : null}
      <ItemList openModal={props.openModal} cartItems={props.cartItems} />
    </>
  );
}
