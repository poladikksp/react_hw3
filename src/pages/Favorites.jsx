import { ItemList } from "../components/ItemsList/ItemsList";

export function Favorites({ items, favItems, addToFav, removeFromFav, openModal }) {
  return (
    <>
      <h3 className="text-center pt-3 pb-2">Favorite items:</h3>
      {favItems.length === 0 ? <p className="text-center fs-4">Empty...</p> : null}
      <ItemList
        openModal={openModal}
        addToFav={addToFav}
        removeFromFav={removeFromFav}
        items={items}
        favItems={favItems}
      />
    </>
  );
}
