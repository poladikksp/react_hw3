import { AppRoutes } from "./AppRoutes";
import { useState, useEffect } from "react";
import { Modal } from "./components/Modal/Modal";
import { Header } from "./components/Header/Header";

function App() {
  const [items, setItems] = useState([]);
  const [favItems, setFavItems] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [selectedItem, setSelectedItem] = useState({});
  const [modalOpen, setModalOpen] = useState(false);
  const [removingModalOpen, setRemovingModalOpen] = useState(false);

  useEffect(() => {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => setItems(data))
      .catch((error) => console.error(error));
    if (localStorage.getItem("favorites")) {
      setFavItems(JSON.parse(localStorage.getItem("favorites")));
    }
    if (localStorage.getItem("cart")) {
      setCartItems(JSON.parse(localStorage.getItem("cart")));
    }
  }, []);

  function addToFav(e, id) {
    const item = items.find((item) => item.id === id);
    setFavItems((prevState) => {
      const newFavArr = [...prevState, item];
      localStorage.setItem("favorites", JSON.stringify(newFavArr));
      return newFavArr;
    });
  }
  function removeFromFav(e, id) {
    const newFavArr = favItems.filter((item) => item.id !== id);
    localStorage.setItem("favorites", JSON.stringify(newFavArr));
    setFavItems(newFavArr);
  }

  function addToCart(selectedItem) {
    setCartItems((prevState) => {
      const newCartArr = [...prevState, selectedItem];
      localStorage.setItem("cart", JSON.stringify(newCartArr));
      return newCartArr;
    });
  }
  function removeFromCart(selectedItem) {
    const deletingElIndex = cartItems.findIndex((item) => item.id === selectedItem.id);
    const newCartArr = cartItems.filter((item, index) => index !== deletingElIndex);
    localStorage.setItem("cart", JSON.stringify(newCartArr));
    setCartItems(newCartArr);
  }
  function openModal(e, id, removing) {
    setModalOpen(true);
    setSelectedItem(items.find((item) => item.id === id));
    removing ? setRemovingModalOpen(true) : setRemovingModalOpen(false);
  }
  function closeModal() {
    setModalOpen(false);
  }
  return (
    <>
      <Header favItemsCount={favItems.length} cartItemsCount={cartItems.length} />
      <AppRoutes
        items={items}
        favItems={favItems}
        cartItems={cartItems}
        selectedItem={selectedItem}
        addToFav={addToFav}
        removeFromFav={removeFromFav}
        openModal={openModal}
      />
      {modalOpen ? (
        <Modal
          removingModalOpen={removingModalOpen}
          selectedItem={selectedItem}
          removeFromCart={removeFromCart}
          addToCart={addToCart}
          closeModal={closeModal}
        />
      ) : null}
    </>
  );
}
export default App;
