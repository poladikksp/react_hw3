import { FavoritesBtn } from "../FavoritesBtn/FavoritesBtn";
import { CartBtn } from "../CartBtn/CartBtn";
import { Link } from "react-router-dom";

export function Header({ favItemsCount, cartItemsCount }) {
  return (
    <>
      <nav className="navbar bg-light border-bottom ">
        <div className="container">
          <Link className="btn btn-light fs-4 pb-2" to={"/"}>
            Laptops
          </Link>
          <div className="d-flex gap-4">
            <Link to={"/favorites"}>
              <FavoritesBtn favItemsCount={favItemsCount} />
            </Link>
            <Link to={"/cart"}>
              <CartBtn cartItemsCount={cartItemsCount} />
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
}
