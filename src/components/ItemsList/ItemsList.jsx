import { ItemCard } from "../ItemCard/ItemCard";

export function ItemList({ items, favItems, cartItems, addToFav, removeFromFav, openModal }) {
  return (
    <>
      <div className="container">
        <ul className="d-flex justify-content-center mt-3 p-0 flex-wrap gap-3">
          {cartItems
            ? cartItems.map((item, index) => {
                return <ItemCard cartItem={true} openModal={openModal} key={index} item={item} />;
              })
            : items.map((item) => {
                return (
                  <ItemCard
                    openModal={openModal}
                    favorite={favItems.find((favItem) => favItem.id === item.id) ? true : false}
                    addToFav={addToFav}
                    removeFromFav={removeFromFav}
                    key={item.id}
                    item={item}
                  />
                );
              })}
        </ul>
      </div>
    </>
  );
}
